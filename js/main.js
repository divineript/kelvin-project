// mobile sidebar

let header = document.querySelector('.header')
const headerBtn = document.querySelector('.menu__btn')

let body = document.body;

if (header && headerBtn) {
  headerBtn.addEventListener('click', () => {
    header.classList.toggle('open')
    body.classList.toggle('no-scroll')
  })

  header.querySelectorAll('.menu__link').forEach(link => {
    link.addEventListener('click', () => {
      header.classList.remove('open')
      body.classList.remove('no-scroll')
    })
  })
}


//active style for link navigator

let parent = document.querySelector('.menu__list')
let menuItem = parent.querySelectorAll('.menu__link')

parent.addEventListener('click', (event) => {
  
  let target = event.target;
  
  
  if(target.classList.contains('menu__link')) {
    for(let i = 0; i < menuItem.length; i++) {
      menuItem[i].classList.remove('active')
    }

    target.classList.add('active');
  }
  
});

// add & remove class on links when scrolling the page


let scrollpos = window.scrollY
console.log(scrollpos);

const scrollChange = [1 , 614 , 1013 , 3000 ];

window.addEventListener('scroll', function() { 
  scrollpos = window.scrollY;

  for(let i = 0; i < scrollChange.length; i++){
    if (scrollpos >= scrollChange[i]) { 
      for(let i = 0; i < menuItem.length; i++) {
        menuItem[i].classList.remove('active')
      }
      menuItem[i].classList.add('active');
  
    } else { menuItem[i].classList.remove('active') }
  }
})

// scroll to section on page

document.querySelectorAll('a[href^="#"').forEach(link => {

  link.addEventListener('click', function(e) {
      e.preventDefault();

      let href = this.getAttribute('href').substring(1);

      const scrollTarget = document.getElementById(href);

      const topOffset = document.querySelector('.header').offsetHeight;
      const elementPosition = scrollTarget.getBoundingClientRect().top;
      const offsetPosition = elementPosition - topOffset;

      window.scrollBy({
          top: offsetPosition,
          behavior: 'smooth'
      });
  });
});


// modal window

const modalBtn = document.getElementById('modal-btn')
const modalLink = document.getElementById('modal__link')
let modal = document.querySelector('.modal')

modalBtn.addEventListener('click', function() {
    modal.classList.add('modal-open'),
    body.classList.add('no-scroll')
}) 

modalLink.addEventListener('click', function() {
  modal.classList.add('modal-open'),
  body.classList.add('no-scroll'),
  body.classList.add('no-scroll')
})

modal.addEventListener('click', function() {
  modal.classList.remove('modal-open'),
  body.classList.remove('no-scroll'),
  modalLink.classList.remove('active')
}) 

const closeModalBtn = document.querySelector('.close')

closeModalBtn.addEventListener('click', function() {
    modal.classList.remove('modal-open'),
    body.classList.remove('no-scroll'),
    modalLink.classList.remove('active')
}) 




